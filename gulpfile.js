const { src, dest, watch, parallel, series } = require("gulp");
const sass = require('gulp-sass')(require('sass'));
const concat = require("gulp-concat");
const uglify = require('gulp-uglify-es').default;
const browserSync = require("browser-sync");
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const del = require('del');




const scrPath = "src/";
const distPath = "dist/";

const paths = {
    styles: {
        src: `${scrPath}scss/**/*.scss`,
        dest: `${distPath}css/`
    },
    scripts: {
        src: `${scrPath}js/**/*.js`,
        dest: `${distPath}js/`
    },
    html:{
        src:`${scrPath}index.html`,
        dest: distPath
    },
    img:{
        src:`${scrPath}img/**/*`,
        dest: `${distPath}/img/`
    }
};
function styles() {
    return src(paths.styles.src)
        .pipe(sass({ outputStyle: "compressed" }))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(concat('style.min.css'))
        .pipe(dest(paths.styles.dest))
        .pipe(browserSync.stream());

}


function scripts(){
    return src ([
        /*"node_modules/jquery/dist/jquery.js",*/
        paths.scripts.src
    ])
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(dest(paths.scripts.dest))
        .pipe(browserSync.stream());
}
function watching (){
    // watch(["src/scss/**/*.scss"],styles)
    watch([paths.styles.src], styles)
    // watch(["src/js/app.js"], scripts)
    watch([paths.scripts.src], scripts)
    // watch(["src/*.html"]).on("change", browserSync.reload)
    watch([paths.html.src], htmlFile)
}
function htmlFile (){
    return src(paths.html.src)
        .pipe(dest(paths.html.dest))
        .pipe(browserSync.stream());
}
function reloadPage(){
    browserSync.init({
        server: {
            baseDir: distPath,
            port: 3000,
            keepalive:true
        }
    })
}
function imgMin() {
    return src(paths.img.src)
        .pipe(imagemin())
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(dest(paths.img.dest))
        .pipe(browserSync.stream());

}

function delDist () {
    return del(distPath);
}
// exports.styles = styles;
// exports.scripts = scripts;
// exports.watching = watching;
// exports.imgMin = imgMin;
// exports.reloadPage = reloadPage;
exports.delDist = delDist;
exports.default = series (delDist, parallel(reloadPage, watching, htmlFile, scripts, imgMin, styles));